import cv2
from PyQt5 import QtCore
from PyQt5.QtGui import QImage
from PyQt5.QtCore import Qt, pyqtSignal
from on_timer import OnTimer
from imutils.video import FPS


class CameraWorker(QtCore.QObject):
    image_update = pyqtSignal(QImage)
    msg = pyqtSignal(list)
    finished = pyqtSignal()
    fps_value = pyqtSignal(float)

    def __init__(self, src):
        super().__init__()
        self.cam_source = src
        self.fup = OnTimer()
        self.stop_cam = False

    def run(self):
        cam = cv2.VideoCapture(self.cam_source, cv2.CAP_DSHOW)
        if cam is None or not cam.isOpened():
            self.msg.emit([0, "Cannot open the camera!"])
        else:
            fps = FPS().start()
            while not self.stop_cam:
                ret, frame = cam.read()
                if ret:
                    frame = cv2.flip(frame, 1)
                    img = self.fup.run(frame, True)
                    convert_to_qt_format = QImage(img.data, img.shape[1], img.shape[0],
                                                  QImage.Format_BGR888)
                    img = convert_to_qt_format.scaled(640, 480, Qt.KeepAspectRatio)
                    self.image_update.emit(img)
                    fps.update()
                    fps.stop()
                    self.fps_value.emit(fps.fps())
            else:
                cam.release()
                cv2.destroyAllWindows()
                self.finished.emit()
