import os
import cv2
import imutils
from datetime import datetime
from imutils.video import FPS
from imutils.video import WebcamVideoStream
from frame_processing import FrameProcessing

TEST_MODE = False
# SOURCE = "rtsp://192.72.1.1/liveRTSP/v1/?"
SOURCE = 0


def single_image_test(fup, src):
    frame = cv2.imread(src)
    # frame = cv2.flip(frame, 1)
    # frame = imutils.resize(frame, width=640, height=360)
    # img = fup.run(frame, True)
    _, _, img = fup.run_center_point_test(frame, True)
    cv2.imshow("Frame", img)  # show captured frame
    cv2.waitKey(0)


def camera_streaming_test(fup, src):
    # For test mode init
    img_ori_stored = []
    img_drawn_stored = []
    subfolder_name = ""
    if TEST_MODE:
        # Check main output folder
        if not os.path.exists("output_img"):
            os.makedirs("output_img")

        # Create subdir output folder
        datetime_str = datetime.now().strftime("%Y%m%d_%H%M%S")
        subfolder_name = datetime_str
        os.makedirs("output_img/" + subfolder_name)
        # Create ori img folder
        os.makedirs("output_img/" + subfolder_name + "/ori_img")
        # Create drawn img folder
        os.makedirs("output_img/" + subfolder_name + "/drawn_img")

    # Create an object to hold reference to camera video capturing
    vs = WebcamVideoStream(src=src).start()
    fps = FPS().start()

    while True:
        frame = vs.read()
        # frame = cv2.flip(frame, 1)

        # img = frame
        img = fup.run(frame, True)
        # _, _, img = fup.run_center_point_test(frame, True)
        # _, _, _, _, img = fup.run_resolution_test(frame, True)
        # _, img = fup.run_rotation_test(frame, True)
        # _, _, _, img = fup.run_color_test(frame, True)
        # _, img = fup.run_mirror_test(frame, True)
        cv2.imshow("Frame", img)  # show captured frame

        if TEST_MODE:
            # Store img
            img_ori_stored.append(frame)
            img_drawn_stored.append(img)

        # press 'q' to break out of the loop
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        fps.update()
    vs.stop()
    fps.stop()
    print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
    cv2.destroyAllWindows()
    # vs.stop()

    if TEST_MODE:
        # Save img
        if len(img_ori_stored) != 0:
            for idx, img in enumerate(img_ori_stored):
                img_name = f"img_{idx}.jpg"
                cv2.imwrite("output_img/" + subfolder_name + "/ori_img/" + img_name, img)

            for idx, img in enumerate(img_drawn_stored):
                img_name = f"img_{idx}.jpg"
                cv2.imwrite("output_img/" + subfolder_name + "/drawn_img/" + img_name, img)


def main():
    # Init class
    fup = FrameProcessing()

    """
    Camera Stream
    """
    camera_streaming_test(fup, SOURCE)

    """
    Single Image
    """
    # single_image_test(fup, "IMG150101-102047-000006F.JPG")


if __name__ == "__main__":
    # Main
    main()
