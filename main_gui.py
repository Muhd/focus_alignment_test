from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import QThread
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QMessageBox
from ui.main_window.main_window import Ui_MainWindow
from app_script.camera_worker import CameraWorker


RTSP_LINK = "rtsp://192.72.1.1/liveRTSP/v1/?"


class MainWindow(QtWidgets.QMainWindow):
    """
    MAIN WINDOW UI
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        """
        INIT GUI WINDOW
        """
        system_log = "Building interface..."
        print(system_log)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        """
        INIT CONFIGURATION DATA/VARIABLES
        """
        system_log = "Initialize configuration data..."
        print(system_log)
        # Init test thread, worker
        self.cam_thread = QThread()
        self.cam_worker = CameraWorker(0)

        """
        CONNECT SIGNAL
        """
        system_log = "Connect signal..."
        print(system_log)

        # Start camera
        system_log = "Starting camera..."
        print(system_log)
        self.start_cam_view()

    def image_update_slot(self, img):
        self.ui.cam_view_lab.setPixmap(QPixmap.fromImage(img))

    def show_message_slot(self, msg):
        if msg[0] == 0:
            self.show_alert_popup(msg[1])

    def show_fps(self, fps):
        self.ui.fps_lab.setText("{:.2f}".format(fps))

    def start_cam_view(self):
        self.cam_thread = QThread()
        self.cam_worker = CameraWorker(0)
        self.cam_worker.moveToThread(self.cam_thread)
        self.cam_thread.started.connect(self.cam_worker.run)
        self.cam_worker.finished.connect(self.cam_thread.quit)
        self.cam_thread.finished.connect(self.cam_thread.deleteLater)
        self.cam_worker.image_update.connect(self.image_update_slot)
        self.cam_worker.msg.connect(self.show_message_slot)
        self.cam_worker.fps_value.connect(self.show_fps)

        # Start the thread
        self.cam_thread.start()

    @staticmethod
    def show_alert_popup(val):
        msg = QMessageBox()
        msg.setWindowTitle("Error")
        msg.setText(val)
        msg.setIcon(QMessageBox.Critical)
        msg.setWindowModality(QtCore.Qt.NonModal)
        msg.exec_()

    def closeEvent(self, event):
        close = QtWidgets.QMessageBox.question(self,
                                               "QUIT",
                                               "Are you sure want to stop process?",
                                               QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        if close == QtWidgets.QMessageBox.Yes:
            self.cam_worker.stop_cam = True
            if not self.cam_thread.isFinished():
                self.cam_thread.quit()
                self.cam_thread.wait()
            event.accept()
        else:
            event.ignore()


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    window = MainWindow()
    window.show()
    app.exec_()
