import cv2
# import sys
import numpy as np
# import matplotlib
from operator import itemgetter
import matplotlib.pyplot as plt
from multiprocessing import Pool
from scipy.signal import argrelextrema


SMA_LENGTH = 10
FRAME_CENTER = [300, 240]
FRAME_CENTER_TOLERANCE_PERCENTAGE = 0.2
FRAME_CENTER_TOLERANCE = [150.0, 80.0]
MAX_CONTOUR_AREA = 5500
MIN_CONTOUR_AREA = 1000
FOV_MIN_CONTOUR_AREA = 100
FOV_MAX_CONTOUR_AREA = 2500
MIN_SQUARE_AREA_RATIO = 0.5  # 0.8
MAX_SQUARE_AREA_RATIO = 1.5  # 1.2
CENTER_AREA_PERCENTAGE = 0.05
MIN_TVL_CONTRAST = 0.35
MAX_TVL = 500
MTF_RANGE = 300
TVL_WINDOW_SIZE = 2
TVL_AVG_LEN = 3
TVL_ROI_WIDTH = 45  # 50
V_TVL_ROI_HEIGHT = 150  # 180
H_TVL_ROI_HEIGHT = 150  # 180
CENTER_TO_TVL_DISTANCE = 35  # 35
FOV_ROI_WIDTH = 50  # 50
V_FOV_ROI_HEIGHT = 40  # 180
H_FOV_ROI_HEIGHT = 50  # 180
H_CENTER_TO_FOV_DISTANCE = 210  # 35
V_CENTER_TO_FOV_DISTANCE = 190  # 35
OVERLAY_TOP_ROI_X = 258
OVERLAY_TOP_ROI_Y = 125
OVERLAY_TOP_ROI_WIDTH = 4
OVERLAY_TOP_ROI_HEIGHT = 4
OVERLAY_CENTER_ROI_X = 258
OVERLAY_CENTER_ROI_Y = 192
OVERLAY_CENTER_ROI_WIDTH = 4
OVERLAY_CENTER_ROI_HEIGHT = 4
OVERLAY_BOTTOM_ROI_X = 258
OVERLAY_BOTTOM_ROI_Y = 368
OVERLAY_BOTTOM_ROI_WIDTH = 4
OVERLAY_BOTTOM_ROI_HEIGHT = 4
OVERLAY_TOP_BLUE_SPEC = [80, 100]
OVERLAY_TOP_GREEN_SPEC = [100, 130]
OVERLAY_TOP_RED_SPEC = [70, 90]
OVERLAY_CENTER_BLUE_SPEC = [150, 160]
OVERLAY_CENTER_GREEN_SPEC = [150, 180]
OVERLAY_CENTER_RED_SPEC = [70, 90]
OVERLAY_BOTTOM_BLUE_SPEC = [150, 255]
OVERLAY_BOTTOM_GREEN_SPEC = [150, 170]
OVERLAY_BOTTOM_RED_SPEC = [120, 140]
CR = 0
CL = 1
CT = 2
CB = 3
LC = 0
RC = 1
TC = 2
BC = 3
ROI_TOP = 0
ROI_CENTER = 1
ROI_BOTTOM = 2
BLUE_HUE_VALUE = 170
GREEN_HUE_VALUE = 85
RED_HUE_VALUE = 255
COLOR_TEST_PIXEL_EXPANSION = 10
AVG_PIXEL_VALUE_LIMIT = (100, 120)
THRESHOLD_VALUE = 125
MAX_THRESHOLD_VALUE = 255
# PARKING GUIDELINE TEST
PGL_TOLERANCE = 1.0
PGL_THRESHOLD_VALUE = 30
PGL_MAX_THRESHOLD_VALUE = 255
PGL_MAX_CONTOUR_AREA = 500
PGL_MAX_FILTERED_CONTOUR = 50
PGL_POINT1 = [[150, 327], [153, 322]]  # PGL_POINT = [list of extreme point]
PGL_POINT2 = [[205, 243], [208, 238]]
PGL_POINT3 = [[242, 183], [246, 178]]
PGL_POINT4 = [[261, 323]]
PGL_POINT5 = [[276, 240]]
PGL_POINT6 = [[291, 179]]
PGL_POINT7 = [[340, 179]]
PGL_POINT8 = [[359, 240]]
PGL_POINT9 = [[378, 323]]
PGL_POINT10 = [[386, 174], [388, 178]]
PGL_POINT11 = [[427, 232], [431, 237]]
PGL_POINT12 = [[486, 313], [489, 318]]
ALL_PGL_POINTS = [PGL_POINT1, PGL_POINT2, PGL_POINT3, PGL_POINT4, PGL_POINT5, PGL_POINT6, PGL_POINT7,
                         PGL_POINT8, PGL_POINT9, PGL_POINT10, PGL_POINT11, PGL_POINT12]

# BGR color
GREEN_COLOR = (0, 255, 0)
WHITE_COLOR = (255, 255, 255)
RED_COLOR = (0, 0, 255)
BLUE_COLOR = (255, 0, 0)


class FrameProcessing:
    def __init__(self):
        self.data_log = {"Sample ID": "", "Center X": "", "Center Y": "", "Resolution-T": "", "Resolution-B": "",
                         "Resolution-L": "",  "Resolution-R": "", "Display Mirror": "", "Delta Red": "",
                         "Delta Green": "",  "Delta Blue": "", "Rotation": ""}
        self.tvl_roi_crop = {}
        self.tvl_vals = {}
        self.tvl_locations = {}
        self.tvl_first_run = True
        self.avg_tvl_vals = {}
        self.avg_tvl_locations = {}
        self.fov_vals = {}
        self.fov_locations = {}
        self.fov_roi_crop = {}
        self.overlay_roi_crop = {}
        self.overlay_vals = {}
        self.overlay_exists = {}

        # For run ALL test flag
        self.run_all = False
        self.run_all_info = {}

    def run(self, src, draw=False):
        # Run all flag
        self.run_all = True

        # Run center point test
        _, _, draw_img = self.run_center_point_test(src, draw)

        # Run resolution test
        _, _, _, _, draw_img = self.run_resolution_test(src, draw, draw_img)

        # Run rotation test
        _, draw_img = self.run_rotation_test(src, draw, draw_img)

        # Run color test
        _, _, _, draw_img = self.run_color_test(src, draw, draw_img)

        # Run mirror test
        _, draw_img = self.run_mirror_test(src, draw, draw_img)

        # Run FOV test
        _, _, _, _, draw_img = self.run_fov_test(src, draw, draw_img)

        # Run overlay test
        _, draw_img = self.run_overlay_test(src, draw, draw_img)

        # Run parking guideline test
        _, draw_img = self.run_parking_guideline_test(src, draw, draw_img)

        # Reset run all info
        self.run_all_info = {}

        return draw_img

    def run_center_point_test(self, src, draw=False, drawn_img=None):
        # Get a copy of the original frame
        ori_frame = src.copy()

        # Specify center point of captured frame
        self._update_frame_center(ori_frame)

        # Get shape detection
        square, circle = self._get_shape_detection(ori_frame)
        # print(square)

        # Get center point of circle
        center_x = None
        center_y = None
        if len(circle) > 0:
            for cl in circle.keys():
                center_x = circle[cl][0]
                center_y = circle[cl][1]

            # Update to data log
            self.data_log["Center X"] = center_x - FRAME_CENTER[0]
            self.data_log["Center Y"] = center_y - FRAME_CENTER[1]

        # Draw
        draw_frame = ori_frame
        if draw:
            if len(circle) > 0:
                if drawn_img is not None:
                    draw_frame = drawn_img

                for cl in circle.keys():
                    draw_frame = self._draw_center_point_info(ori_frame, circle[cl][0], circle[cl][1],
                                                              circle[cl][2], circle[cl][3], circle[cl][4])
        else:
            if drawn_img is not None:
                draw_frame = drawn_img
            else:
                draw_frame = ori_frame

        if self.run_all:
            self.run_all_info = {"center_x": center_x, "center_y": center_y, "square_detection": square}

        # Specify value to return
        if center_x is not None and center_y is not None:
            center_x = center_x - FRAME_CENTER[0]
            center_y = center_y - FRAME_CENTER[1]

        return center_x, center_y, draw_frame

    def tvl_test_multiprocessing(self, chart_id_list, center_x, center_y, ori_frame):
        # Retrieve parameter need to pass
        tvl_list = []
        for chart_id in chart_id_list:
            tvl_list.append((ori_frame, chart_id, center_x, center_y))

        # Start processing
        with Pool(processes=4) as pool:
            outputs = pool.starmap(self._tvl_test, tvl_list)

        # Retrieve return value
        for i, return_val in enumerate(outputs):
            self.tvl_vals[chart_id_list[i]] = return_val[0]
            self.tvl_locations[chart_id_list[i]] = return_val[1]

    def run_resolution_test(self, src, draw=False, drawn_img=None, min_cr=350, min_cl=350, min_ct=350,
                            min_cb=350, max_cr=MAX_TVL, max_cl=MAX_TVL, max_ct=MAX_TVL, max_cb=MAX_TVL):

        # Get a copy of the original frame
        ori_frame = src.copy()

        # Specify center point of captured frame
        self._update_frame_center(ori_frame)

        if not self.run_all:
            # Get shape detection
            _, circle = self._get_shape_detection(ori_frame)

            # Get center point circle
            center_x = None
            center_y = None
            if len(circle) > 0:
                for cl in circle.keys():
                    center_x = circle[cl][0]
                    center_y = circle[cl][1]
        else:
            center_x = self.run_all_info["center_x"]
            center_y = self.run_all_info["center_y"]

        # TVL test
        draw_img = ori_frame
        if center_x is not None and center_y is not None:
            # Run tvl test in multi processing
            # self.tvl_test_multiprocessing([CR, CL, CT, CB], center_x, center_y, ori_frame)

            self.tvl_vals[CR], self.tvl_locations[CR] = self._tvl_test(ori_frame, CR, center_x, center_y)
            self.tvl_vals[CL], self.tvl_locations[CL] = self._tvl_test(ori_frame, CL, center_x, center_y)
            self.tvl_vals[CT], self.tvl_locations[CT] = self._tvl_test(ori_frame, CT, center_x, center_y)
            self.tvl_vals[CB], self.tvl_locations[CB] = self._tvl_test(ori_frame, CB, center_x, center_y)

            # Apply simple moving average
            self._tvl_sma()

            # Update to data log
            self.data_log["Resolution-R"] = self.tvl_vals[CR]
            self.data_log["Resolution-L"] = self.tvl_vals[CL]
            self.data_log["Resolution-T"] = self.tvl_vals[CT]
            self.data_log["Resolution-B"] = self.tvl_vals[CB]

            # Draw
            if draw:
                if drawn_img is not None:
                    draw_frame = drawn_img
                else:
                    draw_frame = ori_frame
                min_spec_tvl_val = [min_cr, min_cl, min_ct, min_cb]
                max_spec_tvl_val = [max_cr, max_cl, max_ct, max_cb]
                draw_img = self._draw_tvl_info(draw_frame, min_spec_tvl_val, max_spec_tvl_val)
        else:
            self.tvl_vals[CR], self.tvl_locations[CR] = None, None
            self.tvl_vals[CL], self.tvl_locations[CL] = None, None
            self.tvl_vals[CT], self.tvl_locations[CT] = None, None
            self.tvl_vals[CB], self.tvl_locations[CB] = None, None

        return self.tvl_vals[CR], self.tvl_vals[CL], self.tvl_vals[CT], self.tvl_vals[CB], draw_img

    def run_rotation_test(self, src, draw=False, drawn_img=None):
        # Get a copy of the original frame
        ori_frame = src.copy()

        # Specify center point of captured frame
        self._update_frame_center(ori_frame)

        if not self.run_all:
            # Get shape detection
            square, _ = self._get_shape_detection(ori_frame)
        else:
            square = self.run_all_info["square_detection"]

        # Rotation test info
        rotated_angle = None

        # Get rotation
        draw_frame = ori_frame
        if len(square) == 4:
            rotated_contour, rotated_angle = self._rotation_test(square)

            # Update result
            self.data_log["Rotation"] = "{:.2f} deg".format(rotated_angle)

            # Store in run all info
            self.run_all_info["rotated_contour"] = rotated_contour

            if draw:
                if drawn_img is not None:
                    draw_frame = drawn_img

                for sq in square.keys():
                    draw_frame = self._draw_detected_square_center_point(draw_frame, square[sq][0], square[sq][1],
                                                                         square[sq][2], square[sq][3],
                                                                         square[sq][4])

                draw_frame = self._draw_rotation_info(draw_frame, rotated_contour, rotated_angle)
            else:
                draw_frame = ori_frame
        else:
            draw_frame = ori_frame

        return rotated_angle, draw_frame

    def run_color_test(self, src, draw=False, drawn_img=None):
        # Get a copy of the original frame
        ori_frame = src.copy()

        # Specify center point of captured frame
        self._update_frame_center(ori_frame)

        if not self.run_all:
            # Get shape detection
            square, _ = self._get_shape_detection(ori_frame)
        else:
            square = self.run_all_info["square_detection"]

        # Color test info
        delta_blue = None
        delta_green = None
        delta_red = None

        # Get rotation
        draw_frame = ori_frame
        if len(square) == 4:
            if not self.run_all:
                rotated_contour, _ = self._rotation_test(square)
            else:
                rotated_contour = self.run_all_info["rotated_contour"]

            delta_blue, delta_green, delta_red = self._color_test(ori_frame, rotated_contour)

            # Update result
            self.data_log["Delta Red"] = f"{delta_red}"
            self.data_log["Delta Green"] = f"{delta_green}"
            self.data_log["Delta Blue"] = f"{delta_blue}"

            # Store info in run all info
            self.run_all_info["delta_red"] = delta_red

            # Draw
            if draw:
                if drawn_img is not None:
                    draw_frame = drawn_img
                draw_img = self._draw_color_info(draw_frame, rotated_contour, delta_blue, delta_green, delta_red)
            else:
                draw_img = ori_frame
        else:
            draw_img = ori_frame

        return delta_blue, delta_green, delta_red, draw_img

    def run_mirror_test(self, src, draw=False, drawn_img=None):
        # Get a copy of the original frame
        ori_frame = src.copy()

        # Specify center point of captured frame
        self._update_frame_center(ori_frame)

        if not self.run_all:
            # Get shape detection
            square, _ = self._get_shape_detection(ori_frame)
        else:
            square = self.run_all_info["square_detection"]

        draw_frame = ori_frame
        # Get rotation
        if len(square) == 4:
            if not self.run_all:
                rotated_contour, _ = self._rotation_test(square)
                _, _, delta_red = self._color_test(ori_frame, rotated_contour)
            else:
                delta_red = self.run_all_info["delta_red"]

            mirrored = self._mirror_test(delta_red)
            mirrored_str = "Mirrored" if mirrored else "Not mirrored"
            self.data_log["Display Mirror"] = mirrored_str

            # Draw
            if draw:
                if drawn_img is not None:
                    draw_frame = drawn_img
                draw_img = self._draw_mirror_info(draw_frame, mirrored, mirrored_str)
            else:
                draw_img = ori_frame
        else:
            mirrored = None
            draw_img = ori_frame

        return mirrored, draw_img

    def run_fov_test(self, src, draw=False, drawn_img=None, min_lc=200, min_rc=200, min_tc=200,
                            min_bc=200, max_lc=500, max_rc=500, max_tc=500, max_bc=500):
        # Get a copy of the original frame
        ori_frame = src.copy()

        # Specify center point of captured frame
        self._update_frame_center(ori_frame)

        if not self.run_all:
            # Get shape detection
            _, circle = self._get_shape_detection(ori_frame)

            # Get center point circle
            center_x = None
            center_y = None
            if len(circle) > 0:
                for cl in circle.keys():
                    center_x = circle[cl][0]
                    center_y = circle[cl][1]
        else:
            center_x = self.run_all_info["center_x"]
            center_y = self.run_all_info["center_y"]

        # FOV test
        draw_img = ori_frame
        if center_x is not None and center_y is not None:

            self.fov_vals[LC], self.fov_locations[LC] = self._fov_test(ori_frame, LC, center_x, center_y)
            self.fov_vals[RC], self.fov_locations[RC] = self._fov_test(ori_frame, RC, center_x, center_y)
            self.fov_vals[TC], self.fov_locations[TC] = self._fov_test(ori_frame, TC, center_x, center_y)
            self.fov_vals[BC], self.fov_locations[BC] = self._fov_test(ori_frame, BC, center_x, center_y)

            # Update to data log
            self.data_log["FOV L-C"] = self.fov_vals[LC]
            self.data_log["FOV R-C"] = self.fov_vals[RC]
            self.data_log["FOV T-C"] = self.fov_vals[TC]
            self.data_log["FOV B-C"] = self.fov_vals[BC]

            # Draw
            if draw:
                if drawn_img is not None:
                    draw_frame = drawn_img
                else:
                    draw_frame = ori_frame
                min_spec_fov_val = [min_lc, min_rc, min_tc, min_bc]
                max_spec_fov_val = [max_lc, max_rc, max_tc, max_bc]
                draw_img = self._draw_fov_info(draw_frame, min_spec_fov_val, max_spec_fov_val)
        else:
            self.fov_vals[LC], self.fov_locations[LC] = None, None
            self.fov_vals[RC], self.fov_locations[RC] = None, None
            self.fov_vals[TC], self.fov_locations[TC] = None, None
            self.fov_vals[BC], self.fov_locations[BC] = None, None

        return self.fov_vals[LC], self.fov_vals[RC], self.fov_vals[TC], self.fov_vals[BC], draw_img

    def run_overlay_test(self, src, draw=False, drawn_img=None, top_x=OVERLAY_TOP_ROI_X, top_y=OVERLAY_TOP_ROI_Y,
                         top_width=OVERLAY_TOP_ROI_WIDTH, top_height=OVERLAY_TOP_ROI_HEIGHT,
                         center_x=OVERLAY_CENTER_ROI_X, center_y=OVERLAY_CENTER_ROI_Y,
                         center_width=OVERLAY_CENTER_ROI_WIDTH, center_height=OVERLAY_CENTER_ROI_HEIGHT,
                         bottom_x=OVERLAY_BOTTOM_ROI_X, bottom_y=OVERLAY_BOTTOM_ROI_Y,
                         bottom_width=OVERLAY_BOTTOM_ROI_WIDTH, bottom_height=OVERLAY_BOTTOM_ROI_HEIGHT):

        # Get a copy of the original frame
        ori_frame = src.copy()

        # Specify center point of captured frame
        self._update_frame_center(ori_frame)

        # Get overlay_roi
        self.overlay_roi_crop[ROI_TOP] = [top_x, top_y, top_width, top_height]
        self.overlay_roi_crop[ROI_CENTER] = [center_x, center_y, center_width, center_height]
        self.overlay_roi_crop[ROI_BOTTOM] = [bottom_x, bottom_y, bottom_width, bottom_height]

        # Run test
        ov_blue, ov_green, ov_red = self._overlay_test(ori_frame, ROI_TOP)
        self.overlay_vals[ROI_TOP] = [ov_blue, ov_green, ov_red]
        ov_blue, ov_green, ov_red = self._overlay_test(ori_frame, ROI_CENTER)
        self.overlay_vals[ROI_CENTER] = [ov_blue, ov_green, ov_red]
        ov_blue, ov_green, ov_red = self._overlay_test(ori_frame, ROI_BOTTOM)
        self.overlay_vals[ROI_BOTTOM] = [ov_blue, ov_green, ov_red]

        # Specify overlay exists/not exists
        overall_overlay_exists = self._specify_overlay_exists()

        # Record
        self.data_log["Overlay Exists"] = overall_overlay_exists

        # Draw
        draw_frame = ori_frame
        if draw:
            if drawn_img is not None:
                draw_frame = drawn_img
            draw_img = self._draw_overlay_info(draw_frame)
        else:
            draw_img = ori_frame

        return overall_overlay_exists, draw_img

    def run_parking_guideline_test(self, src, draw=False, drawn_img=None):
        # Get a copy of the original frame
        ori_frame = src.copy()

        # Specify center point of captured frame
        self._update_frame_center(ori_frame)

        # Run parking guideline test
        pgl_result, pgl_detection_dict, pgl_contained_result_list = self._parking_guideline_test(ori_frame)

        # Draw
        draw_frame = ori_frame
        if draw:
            if drawn_img is not None:
                draw_frame = drawn_img
            draw_img = self._draw_parking_guideline_info(draw_frame, pgl_detection_dict, pgl_result,
                                                         pgl_contained_result_list)
        else:
            draw_img = ori_frame
        return pgl_result, draw_img

    def _tvl_test(self, img, chart_id, center_x, center_y):
        if chart_id == CR:
            x = int(center_x + CENTER_TO_TVL_DISTANCE)
            y = int(center_y - (TVL_ROI_WIDTH / 2))
            w = int(H_TVL_ROI_HEIGHT)
            h = int(TVL_ROI_WIDTH)
            roi_crop = img[y:y+h, x:x+w].copy()
            roi_crop = cv2.cvtColor(roi_crop, cv2.COLOR_BGR2GRAY)

        elif chart_id == CL:
            x = int(center_x - CENTER_TO_TVL_DISTANCE - H_TVL_ROI_HEIGHT)
            new_x = 1 if x <= 0 else x
            y = int(center_y - (TVL_ROI_WIDTH / 2))
            new_y = 1 if y <= 0 else y
            w = int(H_TVL_ROI_HEIGHT)
            h = int(TVL_ROI_WIDTH)
            roi_crop = img[new_y:h + y, new_x:w + x].copy()
            roi_crop = cv2.cvtColor(roi_crop, cv2.COLOR_BGR2GRAY)
            roi_crop = cv2.flip(roi_crop, 1)

        elif chart_id == CT:
            x = int(center_x - (TVL_ROI_WIDTH / 2))
            new_x = 1 if x <= 0 else x
            y = int(center_y - CENTER_TO_TVL_DISTANCE - V_TVL_ROI_HEIGHT)
            new_y = 1 if y <= 0 else y
            w = int(TVL_ROI_WIDTH)
            h = int(V_TVL_ROI_HEIGHT)
            roi_crop = img[new_y:h + y, new_x:w + x].copy()
            roi_crop = cv2.cvtColor(roi_crop, cv2.COLOR_BGR2GRAY)
            roi_crop = cv2.rotate(roi_crop, cv2.ROTATE_90_CLOCKWISE)

        elif chart_id == CB:
            x = int(center_x - (TVL_ROI_WIDTH / 2))
            y = int(center_y + CENTER_TO_TVL_DISTANCE)
            w = int(TVL_ROI_WIDTH)
            h = int(V_TVL_ROI_HEIGHT)
            roi_crop = img[y:y+h, x:x+w].copy()
            roi_crop = cv2.cvtColor(roi_crop, cv2.COLOR_BGR2GRAY)
            roi_crop = cv2.rotate(roi_crop, cv2.ROTATE_90_COUNTERCLOCKWISE)

        else:
            x = center_x + CENTER_TO_TVL_DISTANCE
            y = center_y - (TVL_ROI_WIDTH / 2)
            w = H_TVL_ROI_HEIGHT
            h = TVL_ROI_WIDTH
            roi_crop = img[y:y+h, x:x+w].copy()
            roi_crop = cv2.cvtColor(roi_crop, cv2.COLOR_BGR2GRAY)

        # Record crop details
        self.tvl_roi_crop[chart_id] = (x, y, w, h)

        # Parameter of sliding window
        windows_n_rows = TVL_ROI_WIDTH
        windows_n_cols = TVL_WINDOW_SIZE

        # Step of each window
        step_slide = TVL_WINDOW_SIZE

        # For detecting TVL
        tvl_location = 0
        tvl_found = False
        min_tvl_contrast_achieved = False
        tvl_start = 0
        tvl_end = 0
        tvl_contrast_list = []
        tvl_contrast_avg_list = []

        # TVL avg pixel list
        tvl_avg_pixel_list = []
        tvl_avg_maxima = []
        tvl_avg_minima = []

        # roi_height = roi_crop.shape[0]
        roi_width = roi_crop.shape[1]
        for col in reversed(range(0, roi_width, step_slide)):
            # Resulting window
            slide_rect = (col, 0, windows_n_cols, windows_n_rows)
            slide_window = roi_crop[slide_rect[1]:slide_rect[1]+slide_rect[3], slide_rect[0]:slide_rect[0] +
                                                                                             slide_rect[2]]

            # Set pixel value
            pixel_val = 0
            avg_pixel_list = []

            # For black line detection
            blackline_found = False
            first_blackline = True
            blackline_start = 0
            blackline_end = 0

            for i in range(0, windows_n_rows):
                # Get pixel average between 2 col per row
                for j in range(0, windows_n_cols):
                    try:
                        pixel_val += slide_window[i, j]
                    except IndexError:  # If pixel width only have 1 pixel
                        pass
                avg_pixel_val = pixel_val / windows_n_cols
                avg_pixel_list.append(avg_pixel_val)

                # Black line detection
                if not blackline_found and avg_pixel_val < AVG_PIXEL_VALUE_LIMIT[0]:
                    blackline_found = True
                    if first_blackline:
                        first_blackline = False
                        blackline_start = i
                elif blackline_found and avg_pixel_val > AVG_PIXEL_VALUE_LIMIT[1]:
                    blackline_found = False
                    blackline_end = i - 1

                # Reset pixel value
                pixel_val = 0
                # print(avg_pixel_val)
                # print(avg_pixel_list)
                # cv2.waitKey(0)

            # print(f"BEFORE: col:({col}), tvl_found({tvl_found}), tvl_start({tvl_start}), tvl_end({tvl_end}),
            # min_avg_pixel({min(avg_pixel_list)})")
            # TVL detection
            if tvl_start == 0 and min(avg_pixel_list) < AVG_PIXEL_VALUE_LIMIT[0]:
                tvl_found = True
                tvl_start = col + windows_n_cols
            elif tvl_end == 0 and tvl_start != 0 and min(avg_pixel_list) > AVG_PIXEL_VALUE_LIMIT[1]:
                tvl_end = col + windows_n_cols

            # print(f"AFTERR: col:({col}), tvl_found({tvl_found}), tvl_start({tvl_start}), tvl_end({tvl_end}),
            # min_avg_pixel({min(avg_pixel_list)})")
            # print(chart_id, blackline_found, blackline_start, blackline_end)

            avg_local_maxima = 0
            avg_local_minima = 0
            local_maxima_point_found = False
            local_minima_point_found = False
            if tvl_found and tvl_end == 0 and blackline_start != 0 and blackline_end != 0:
                avg_pixel_np_array = np.array(avg_pixel_list)
                # print("IN TVL")
                # For local maxima
                local_maxima = avg_pixel_np_array[argrelextrema(avg_pixel_np_array, np.greater)[0]]
                # print(f"{local_maxima.size}, {local_maxima}")
                if local_maxima.size >= 2:
                    avg_local_maxima = np.average(local_maxima)
                    # print("avg mx", avg_local_maxima)
                    local_maxima_point_found = True
                    tvl_avg_maxima.append(avg_local_maxima)

                # For local minima
                local_minima = avg_pixel_np_array[argrelextrema(avg_pixel_np_array, np.less)[0]]
                # print(f"{local_minima.size}, {local_minima}")
                if local_minima.size >= 2:
                    avg_local_minima = np.average(local_minima)
                    # print("avg mn", avg_local_minima)
                    local_minima_point_found = True
                    tvl_avg_minima.append(avg_local_minima)

                # self.plot_local_max_min(avg_pixel_np_array)

            if local_maxima_point_found and local_minima_point_found:
                # print(f"AVG MAX :{avg_local_maxima}, AVG MIN :{avg_local_minima}")
                tvl_contrast = (avg_local_maxima - avg_local_minima) / (avg_local_maxima + avg_local_minima)
                tvl_contrast_list.append(tvl_contrast)
                # print(f"CONTRAST :{tvl_contrast}")
                # print("Contrast: {:.2f}".format(tvl_contrast*100))
                if len(tvl_contrast_list) >= TVL_AVG_LEN:
                    # print(f"3 LAST :{tvl_contrast_list[-TVL_AVG_LEN:]}")
                    tvl_contrast_avg_list.append(sum(tvl_contrast_list[-TVL_AVG_LEN:]) / TVL_AVG_LEN)
                    # print("AVG contrast: {:.2f}".format(tvl_contrast_avg_list[-1] * 100))
                # print(f"AVG CONTRAST LIST :{tvl_contrast_avg_list}")
                if len(tvl_contrast_avg_list) >= 2 and tvl_contrast_avg_list[-1] < MIN_TVL_CONTRAST <= \
                        tvl_contrast_avg_list[-2]:
                    min_tvl_contrast_achieved = True
                    tvl_location = col
                    # print(f"MTF location: {tvl_location}")

            # cv2.imshow("slide", slide_window)
            # self.plot_contrast_col(tvl_contrast_list, title="Contrast ROI column")
            # self.plot_contrast_col(tvl_contrast_avg_list, title="AVG Contrast")
            # if cv2.waitKey(0) & 0xFF == ord('q'):
            #     sys.exit()

            tvl_avg_pixel_list.extend(avg_pixel_list)

        # print(f"TVL found: {tvl_found}, min TVL contrast: {min_tvl_contrast_achieved}")
        # Conclude TVL value
        if tvl_found and min_tvl_contrast_achieved:
            tvl_percent = float(tvl_location - tvl_end) / float(tvl_start - tvl_end)
            tvl_val = MAX_TVL - (tvl_percent * MTF_RANGE)
            # print("first")
        elif len(tvl_contrast_avg_list) > 0 and min(tvl_contrast_list) >= MIN_TVL_CONTRAST:
            tvl_val = 500
            tvl_location = tvl_end
            # print("second")
        else:
            # print("third")
            tvl_val = 0
            tvl_location = tvl_start

        # tvl_avg_pixel_np_array = np.array(tvl_avg_pixel_list)
        # tvl_avg_maxima_np_array = np.array(tvl_avg_maxima)
        # tvl_avg_minima_np_array = np.array(tvl_avg_minima)
        # cv2.imshow("roi", roi_crop)
        # # self.plot_local_max_min(tvl_avg_maxima_np_array)
        # # self.plot_contrast_col(tvl_contrast_avg_list)
        # if cv2.waitKey(0) & 0xFF == ord('q'):
        #     sys.exit()

        # print("MTF value: {:.2f}".format(tvl_val))
        # print("MTF location: {:.2f}".format(tvl_location))

        return tvl_val, tvl_location

    def _tvl_sma(self):
        # if self.tvl_first_run:
        #     for key in self.tvl_vals.keys():
        #         val_list = [self.tvl_vals[key]]
        #         self.avg_tvl_vals[key] = val_list
        #
        #     for key in self.tvl_locations.keys():
        #         loc_list = [self.tvl_locations[key]]
        #         self.avg_tvl_locations[key] = loc_list
        #
        #     self.tvl_first_run = False

        # TVL value sma
        if len(self.avg_tvl_vals) == 0:
            for key in self.tvl_vals.keys():
                val_list = [self.tvl_vals[key]]
                self.avg_tvl_vals[key] = val_list
        else:
            for key in self.avg_tvl_vals.keys():
                tvl_val = self.tvl_vals[key]
                tvl_val_list = self.avg_tvl_vals[key]
                # Check the availability list
                if len(tvl_val_list) == SMA_LENGTH:
                    tvl_val_list.pop(0)
                # Update tvl val
                tvl_val_list.append(tvl_val)
                # Calculate avg
                avg_tvl_val = int((sum(tvl_val_list) / len(tvl_val_list)))
                # Update tvl val
                self.avg_tvl_vals[key] = tvl_val_list
                self.tvl_vals[key] = avg_tvl_val

        # TVL location sma
        if len(self.avg_tvl_locations) == 0:
            for key in self.tvl_locations.keys():
                loc_list = [self.tvl_locations[key]]
                self.avg_tvl_locations[key] = loc_list
        else:
            for key in self.avg_tvl_locations.keys():
                tvl_loc = self.tvl_locations[key]
                tvl_loc_list = self.avg_tvl_locations[key]
                # Check the availability list
                if len(tvl_loc_list) == SMA_LENGTH:
                    tvl_loc_list.pop(0)
                # Update tvl loc
                tvl_loc_list.append(tvl_loc)
                # Calculate avg
                avg_tvl_loc = int((sum(tvl_loc_list)) / len(tvl_loc_list))
                # Update tvl loc
                self.avg_tvl_locations[key] = tvl_loc_list
                self.tvl_locations[key] = avg_tvl_loc

    def _fov_test(self, img, chart_id, img_center_x, img_center_y):
        if chart_id == LC:
            x = int(img_center_x - H_CENTER_TO_FOV_DISTANCE - H_FOV_ROI_HEIGHT)
            new_x = 1 if x <= 0 else x
            y = int(img_center_y - (FOV_ROI_WIDTH / 2))
            new_y = 1 if y <= 0 else y
            w = int(H_FOV_ROI_HEIGHT)
            h = int(FOV_ROI_WIDTH)
            roi_crop = img[new_y:h + y, new_x:w + x].copy()
            roi_crop = cv2.cvtColor(roi_crop, cv2.COLOR_BGR2GRAY)

        elif chart_id == RC:
            x = int(img_center_x + H_CENTER_TO_FOV_DISTANCE)
            y = int(img_center_y - (FOV_ROI_WIDTH / 2))
            w = int(H_FOV_ROI_HEIGHT)
            h = int(FOV_ROI_WIDTH)
            roi_crop = img[y:y + h, x:x + w].copy()
            roi_crop = cv2.cvtColor(roi_crop, cv2.COLOR_BGR2GRAY)

        elif chart_id == TC:
            x = int(img_center_x - (FOV_ROI_WIDTH / 2))
            new_x = 1 if x <= 0 else x
            y = int(img_center_y - V_CENTER_TO_FOV_DISTANCE - V_FOV_ROI_HEIGHT)
            new_y = 1 if y <= 0 else y
            w = int(FOV_ROI_WIDTH)
            h = int(V_FOV_ROI_HEIGHT)
            roi_crop = img[new_y:h + y, new_x:w + x].copy()
            roi_crop = cv2.cvtColor(roi_crop, cv2.COLOR_BGR2GRAY)

        elif chart_id == BC:
            x = int(img_center_x - (FOV_ROI_WIDTH / 2))
            y = int(img_center_y + V_CENTER_TO_FOV_DISTANCE)
            w = int(FOV_ROI_WIDTH)
            h = int(V_FOV_ROI_HEIGHT)
            roi_crop = img[y:y+h, x:x+w].copy()
            roi_crop = cv2.cvtColor(roi_crop, cv2.COLOR_BGR2GRAY)

        else:
            x = img_center_x + H_CENTER_TO_FOV_DISTANCE
            y = img_center_y - (FOV_ROI_WIDTH / 2)
            w = H_FOV_ROI_HEIGHT
            h = FOV_ROI_WIDTH
            roi_crop = img[y:y+h, x:x+w].copy()
            roi_crop = cv2.cvtColor(roi_crop, cv2.COLOR_BGR2GRAY)

        # Record crop details
        self.fov_roi_crop[chart_id] = (x, y, w, h)

        # Convert it to gray
        # gray_frame = cv2.cvtColor(roi_crop, cv2.COLOR_BGR2GRAY)

        # Reduce the noise so we avoid false circle detection
        blurred_frame = cv2.GaussianBlur(roi_crop, (5, 5), 0)
        # cv2.imshow("blur", blurred_frame)

        # Get threshold value
        _, threshold = cv2.threshold(blurred_frame, THRESHOLD_VALUE, MAX_THRESHOLD_VALUE, cv2.THRESH_BINARY)
        # cv2.imshow("circle", threshold)
        # cv2.waitKey()

        # Get contours
        contours, hierarchy = cv2.findContours(threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        circle_detection = {}
        for i, cnt in enumerate(contours):
            # Find shape approximation
            epsilon = cv2.arcLength(cnt, True) * 0.02
            approx = cv2.approxPolyDP(cnt, epsilon, True)

            # for idx, i in enumerate(contours):
            #     print(f"Contour: {cv2.contourArea(i)}")
            #     cv2.imshow("contol", cv2.drawContours(img.copy(), contours, idx, RED_COLOR, 2))
            #     cv2.waitKey()
            # cv2.imshow("cont", cv2.drawContours(img.copy(), contours, -1, RED_COLOR, 2))

            # Remove noise
            if not (FOV_MIN_CONTOUR_AREA < cv2.contourArea(cnt) < FOV_MAX_CONTOUR_AREA) or \
                    not cv2.isContourConvex(approx):
                continue

            if len(approx) > 6:  # Circle
                # Find center of contours
                m = cv2.moments(cnt)
                center_x = int(m["m10"] / m["m00"])
                center_y = int(m["m01"] / m["m00"])
                _, _, w, h = cv2.boundingRect(cnt)
                # radius = w / 2
                # print(f"X: {center_x}, Y: {center_y}")

                # if FRAME_CENTER[0] + FRAME_CENTER_TOLERANCE[0] > center_x > FRAME_CENTER[0] - \
                #         FRAME_CENTER_TOLERANCE[0] and FRAME_CENTER[1] + FRAME_CENTER_TOLERANCE[1] > center_y > \
                #         FRAME_CENTER[1] - FRAME_CENTER_TOLERANCE[1]:

                # Store
                circle_detection[i] = [center_x, center_y, contours, i, hierarchy]

        # Calculate the distance from FOV center point to whole image center point
        if len(circle_detection) != 0:
            if len(circle_detection) == 1:

                # Get stored center point
                circle_center_x = 0
                circle_center_y = 0
                for i in circle_detection.keys():
                    circle_center_x = circle_detection[i][0]
                    circle_center_y = circle_detection[i][1]
                    break

                # Get new coordinate based on whole image
                x = self.fov_roi_crop[chart_id][0]
                y = self.fov_roi_crop[chart_id][1]
                new_center_x = x + circle_center_x
                new_center_y = y + circle_center_y
                fov_location = [new_center_x, new_center_y]

                # Get FOV value
                if chart_id == LC or chart_id == RC:
                    fov_val = abs(new_center_x - img_center_x)
                elif chart_id == TC or chart_id == BC:
                    fov_val = abs(new_center_y - img_center_y)
                else:
                    fov_val = None
            else:
                fov_val = None
                fov_location = None
        else:
            fov_val = None
            fov_location = None

        return fov_val, fov_location

    def _rotation_test(self, square_detected):
        # Get square contour
        square_contour = []
        for key in square_detected.keys():
            center_x = square_detected[key][0]
            center_y = square_detected[key][1]
            point = [center_x, center_y]
            square_contour.append(point)

        # Re-organize points
        square_contour = self._reorder_points(square_contour)

        # Get center of color box in anticlockwise order
        rotated_contour = cv2.convexHull(np.array(square_contour))

        # Get the rotation angle
        angle = cv2.minAreaRect(rotated_contour)[2]
        rotated_angle = 90 + angle if angle < -45 else angle

        return rotated_contour, rotated_angle

    @staticmethod
    def _reorder_points(points_list):

        sorted_x_idx = [i[0] for i in sorted(enumerate(points_list), key=lambda x: x[1])]

        left_most = [sorted_x_idx[0], sorted_x_idx[1]]

        right_most = [sorted_x_idx[2], sorted_x_idx[3]]

        bottom_right = points_list[right_most[0]] if points_list[right_most[0]][1] > \
                                                        points_list[right_most[1]][1] else points_list[
            right_most[1]]

        top_right = points_list[right_most[0]] if points_list[right_most[0]][1] < points_list[right_most[1]][
            1] else points_list[right_most[1]]

        bottom_left = points_list[left_most[0]] if points_list[left_most[0]][1] > points_list[left_most[1]][
            1] else points_list[left_most[1]]

        top_left = points_list[left_most[0]] if points_list[left_most[0]][1] < points_list[left_most[1]][
            1] else points_list[left_most[1]]

        sorted_points_list = [bottom_right, bottom_left, top_left, top_right]

        return sorted_points_list

    @staticmethod
    def _reorder_pgl_points(points_dict):
        # Get x of each contour
        x_axis = [[i, points_dict[i][3]] for i in points_dict.keys()]
        # Sort based on x-axis
        sorted_x_axis = sorted(x_axis, key=itemgetter(1))
        # Get sorted index
        sorted_idx = [i[0] for i in sorted_x_axis]
        # Get sorted points dict
        sorted_points_dict = {}
        for item in sorted_idx:
            sorted_points_dict[item] = points_dict[item]

        return sorted_points_dict

    @staticmethod
    def _color_test(img, rotated_contour):
        # Convert to hsv
        hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV_FULL)

        box_center_point_list = rotated_contour.tolist()

        # Get hue value from box center point pixel
        if len(box_center_point_list) == 4:
            blue_x = box_center_point_list[2][0][0]
            blue_y = box_center_point_list[2][0][1]
            green_x = box_center_point_list[1][0][0]
            green_y = box_center_point_list[1][0][1]
            red_x = box_center_point_list[3][0][0]
            red_y = box_center_point_list[3][0][1]
            blue_hue_value = int(hsv_img[blue_y, blue_x][0])  # Blue box
            green_hue_value = int(hsv_img[green_y, green_x][0])  # Green box
            red_hue_value = int(hsv_img[red_y, red_x][0])  # Red box
        else:
            blue_hue_value = 0
            green_hue_value = 0
            red_hue_value = 0

        # blue_box_idx = 2
        # green_box_idx = 1
        # red_box_idx = 3
        # green_hue_value = 0
        # blue_hue_value = 0
        # red_hue_value = 0
        # expand_pixel = COLOR_TEST_PIXEL_EXPANSION
        # for box in range(4):
        #     if box != 0:
        #         x, y = box_center_point_list[box][0][0] - expand_pixel, box_center_point_list[box][0][1] -
        #                expand_pixel
        #         crop_hsv = hsv_img[y: y + expand_pixel, x: x + expand_pixel]
        #         h, w, _ = crop_hsv.shape
        #         pixel_count = h * w
        #         total_pixel_val = 0
        #         for row in range(h):
        #             for col in range(w):
        #                 val = crop_hsv[row, col][0]
        #                 # print(val)
        #                 total_pixel_val += val
        #         avg_value = int(total_pixel_val / pixel_count)
        #         if box == green_box_idx:
        #             green_hue_value = avg_value
        #         elif box == blue_box_idx:
        #             blue_hue_value = avg_value
        #         elif box == red_box_idx:
        #             red_hue_value = avg_value

        # Get delta value
        delta_blue = 255 - abs(blue_hue_value - BLUE_HUE_VALUE) if abs(blue_hue_value - BLUE_HUE_VALUE) > 127 else abs(
            blue_hue_value - BLUE_HUE_VALUE)
        delta_green = 255 - abs(green_hue_value - GREEN_HUE_VALUE) if abs(green_hue_value - GREEN_HUE_VALUE) > 127 \
            else abs(green_hue_value - GREEN_HUE_VALUE)
        delta_red = 255 - abs(red_hue_value - RED_HUE_VALUE) if abs(red_hue_value - RED_HUE_VALUE) > 127 else abs(
            red_hue_value - RED_HUE_VALUE)

        return delta_blue, delta_green, delta_red

    @staticmethod
    def _mirror_test(delta_red):
        if delta_red > 75:
            return False
        else:
            return True

    def _overlay_test(self, img, roi_id):
        if roi_id == ROI_TOP:
            x = self.overlay_roi_crop[ROI_TOP][0]
            y = self.overlay_roi_crop[ROI_TOP][1]
            w = self.overlay_roi_crop[ROI_TOP][2]
            h = self.overlay_roi_crop[ROI_TOP][3]
            roi_crop = img[y:h + y, x:w + x].copy()
            roi_crop = cv2.cvtColor(roi_crop, cv2.COLOR_BGR2HSV_FULL)
            # cv2.imshow("top", roi_crop)
        elif roi_id == ROI_CENTER:
            x = self.overlay_roi_crop[ROI_CENTER][0]
            y = self.overlay_roi_crop[ROI_CENTER][1]
            w = self.overlay_roi_crop[ROI_CENTER][2]
            h = self.overlay_roi_crop[ROI_CENTER][3]
            roi_crop = img[y:h + y, x:w + x].copy()
            roi_crop = cv2.cvtColor(roi_crop, cv2.COLOR_BGR2HSV_FULL)
            # cv2.imshow("center", roi_crop)
        elif roi_id == ROI_BOTTOM:
            x = self.overlay_roi_crop[ROI_BOTTOM][0]
            y = self.overlay_roi_crop[ROI_BOTTOM][1]
            w = self.overlay_roi_crop[ROI_BOTTOM][2]
            h = self.overlay_roi_crop[ROI_BOTTOM][3]
            roi_crop = img[y:h + y, x:w + x].copy()
            roi_crop = cv2.cvtColor(roi_crop, cv2.COLOR_BGR2HSV_FULL)
            # cv2.imshow("bottom", roi_crop)
        else:
            x = self.overlay_roi_crop[ROI_TOP][0]
            y = self.overlay_roi_crop[ROI_TOP][1]
            w = self.overlay_roi_crop[ROI_TOP][2]
            h = self.overlay_roi_crop[ROI_TOP][3]
            roi_crop = img[y:h + y, x:w + x].copy()
            roi_crop = cv2.cvtColor(roi_crop, cv2.COLOR_BGR2HSV_FULL)
            # cv2.imshow("top", roi_crop)

        # Get average
        avg_color_per_row = np.average(roi_crop, axis=0)
        avg_color = np.average(avg_color_per_row, axis=0)
        avg_blue = avg_color[0]
        avg_green = avg_color[1]
        avg_red = avg_color[2]
        # print(f"{roi_id}: {avg_blue}, {avg_green}, {avg_red}")

        return avg_blue, avg_green, avg_red

    def _parking_guideline_test(self, img):
        # Convert it to gray
        gray_frame = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Reduce the noise so we avoid false circle detection
        blurred_frame = cv2.GaussianBlur(gray_frame, (5, 5), 0)
        # cv2.imshow("blur", blurred_frame)

        # Get threshold value
        _, threshold = cv2.threshold(blurred_frame, PGL_THRESHOLD_VALUE, PGL_MAX_THRESHOLD_VALUE, cv2.THRESH_BINARY)
        # cv2.imshow("thresh", threshold)

        # Find contour
        pgl_contour, _ = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        # print(type(pgl_contour))
        # cv2.imshow("cont", cv2.drawContours(img.copy(), pgl_contour, -1, RED_COLOR, 2))

        # Filter contour
        filtered_pgl_contour = []
        circle_detection = {}
        for i, cnt in enumerate(pgl_contour):
            if not (cv2.contourArea(cnt) < PGL_MAX_CONTOUR_AREA):
                continue

            filtered_pgl_contour.append(cnt)

        point_contained_result_list = []
        if len(filtered_pgl_contour) < PGL_MAX_FILTERED_CONTOUR:
            # Apply agglomerative cluster
            pgl_contour = self._apply_agglomerative_cluster(filtered_pgl_contour)
            for i, cnt in enumerate(pgl_contour):
                x, y, w, h = cv2.boundingRect(cnt)
                center_x = x + (w / 2)
                center_y = y + (h / 2)
                radius = int(PGL_TOLERANCE * (w / 2))
                circle_detection[i] = [center_x, center_y, pgl_contour, x, y, w, h, radius]

            # Re-order contour
            pgl_detection_dict = self._reorder_pgl_points(circle_detection)

            # Specify extreme points
            if len(pgl_detection_dict) == len(ALL_PGL_POINTS):
                pgl_detection_keys_list = [i for i in pgl_detection_dict.keys()]
                for idx, pgl_points in enumerate(ALL_PGL_POINTS):
                    idx_key = pgl_detection_keys_list[idx]
                    detected_center_x = pgl_detection_dict[idx_key][0]
                    detected_center_y = pgl_detection_dict[idx_key][1]
                    # detected_x = pgl_detection_dict[idx_key][3]
                    # detected_y = pgl_detection_dict[idx_key][4]
                    # detected_w = pgl_detection_dict[idx_key][5]
                    # detected_h = pgl_detection_dict[idx_key][6]
                    detected_radius = pgl_detection_dict[idx_key][7]
                    point_result_list = []
                    for points in pgl_points:
                        x = points[0]
                        y = points[1]
                        in_within = (x - detected_center_x) * (x - detected_center_x) + (y - detected_center_y) * \
                                    (y - detected_center_y) <= detected_radius * detected_radius

                        # in_within = detected_x < x < detected_x + detected_w and detected_y < y < detected_y \
                        #             + detected_h

                        # Record
                        point_result_list.append(in_within)

                    # Record
                    point_contained_result_list.append(all(point_result_list))

                # Check result
                if all(point_contained_result_list):
                    pgl_result = True
                else:
                    pgl_result = False
            else:
                pgl_result = None
        else:
            pgl_result = None
            pgl_detection_dict = circle_detection

        return pgl_result, pgl_detection_dict, point_contained_result_list

    def _apply_agglomerative_cluster(self, contours, threshold_distance=20.0):
        current_contours = contours
        while len(current_contours) > 1:
            min_distance = None
            min_coordinate = None

            for x in range(len(current_contours) - 1):
                for y in range(x + 1, len(current_contours)):
                    distance = self._calculate_contour_distance(current_contours[x], current_contours[y])
                    if min_distance is None:
                        min_distance = distance
                        min_coordinate = (x, y)
                    elif distance < min_distance:
                        min_distance = distance
                        min_coordinate = (x, y)

            if min_distance < threshold_distance:
                index1, index2 = min_coordinate
                current_contours[index1] = self._merge_contours(current_contours[index1], current_contours[index2])
                del current_contours[index2]
            else:
                break

        return current_contours

    @staticmethod
    def _calculate_contour_distance(contour1, contour2):
        x1, y1, w1, h1 = cv2.boundingRect(contour1)
        c_x1 = x1 + w1 / 2
        c_y1 = y1 + h1 / 2

        x2, y2, w2, h2 = cv2.boundingRect(contour2)
        c_x2 = x2 + w2 / 2
        c_y2 = y2 + h2 / 2

        return max(abs(c_x1 - c_x2) - (w1 + w2) / 2, abs(c_y1 - c_y2) - (h1 + h2) / 2)

    @staticmethod
    def _merge_contours(contour1, contour2):
        return np.concatenate((contour1, contour2), axis=0)

    @staticmethod
    def _get_shape_detection(img):
        # Convert it to gray
        gray_frame = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Reduce the noise so we avoid false circle detection
        blurred_frame = cv2.GaussianBlur(gray_frame, (5, 5), 0)
        # cv2.imshow("blur", blurred_frame)

        # Get threshold value
        _, threshold = cv2.threshold(blurred_frame, THRESHOLD_VALUE, MAX_THRESHOLD_VALUE, cv2.THRESH_BINARY)
        # cv2.imshow("thresh", threshold)
        # cv2.waitKey()

        # Get contours
        square_detection = {}
        circle_detection = {}
        contours, hierarchy = cv2.findContours(threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        # print(contours)
        # for idx, i in enumerate(contours):
        #     print(f"Contour: {cv2.contourArea(i)}")
        #     cv2.imshow("contol", cv2.drawContours(img.copy(), contours, idx, RED_COLOR, 2))
        #     cv2.waitKey()
        # cv2.imshow("cont", cv2.drawContours(img.copy(), contours, -1, RED_COLOR, 2))
        for i, cnt in enumerate(contours):
            # Find shape approximation
            epsilon = cv2.arcLength(cnt, True) * 0.02
            approx = cv2.approxPolyDP(cnt, epsilon, True)

            # Remove noise
            if not (MIN_CONTOUR_AREA < cv2.contourArea(cnt) < MAX_CONTOUR_AREA) or not cv2.isContourConvex(approx):
                continue
            # print(f"APPROX: {approx}")
            if len(approx) == 4:  # Square
                # Find area ratio
                _, _, w, h = cv2.boundingRect(cnt)
                ar = w / h
                if MIN_SQUARE_AREA_RATIO <= ar <= MAX_SQUARE_AREA_RATIO:
                    # Find center of contours
                    m = cv2.moments(cnt)
                    center_x = int(m["m10"] / m["m00"])
                    center_y = int(m["m01"] / m["m00"])

                    # Store
                    square_detection[i] = [center_x, center_y, contours, i, hierarchy]

            elif len(approx) > 6:  # Circle
                # Find center of contours
                m = cv2.moments(cnt)
                center_x = int(m["m10"] / m["m00"])
                center_y = int(m["m01"] / m["m00"])
                _, _, w, h = cv2.boundingRect(cnt)
                # radius = w / 2
                # print(f"X: {center_x}, Y: {center_y}")

                if FRAME_CENTER[0] + FRAME_CENTER_TOLERANCE[0] > center_x > FRAME_CENTER[0] - \
                        FRAME_CENTER_TOLERANCE[0] and FRAME_CENTER[1] + FRAME_CENTER_TOLERANCE[1] > center_y > \
                        FRAME_CENTER[1] - FRAME_CENTER_TOLERANCE[1]:

                    # Store
                    circle_detection[i] = [center_x, center_y, contours, i, hierarchy]

        return square_detection, circle_detection

    @staticmethod
    def _update_frame_center(frame):
        FRAME_CENTER[1] = int(frame.shape[0] / 2)
        FRAME_CENTER[0] = int(frame.shape[1] / 2)

        FRAME_CENTER_TOLERANCE[0] = float(FRAME_CENTER_TOLERANCE_PERCENTAGE * FRAME_CENTER[0])
        FRAME_CENTER_TOLERANCE[1] = float(FRAME_CENTER_TOLERANCE_PERCENTAGE * FRAME_CENTER[1])

        # print(FRAME_CENTER)
        # print(FRAME_CENTER_TOLERANCE)

    @staticmethod
    def _draw_center_point_info(img, center_x, center_y, contours, idx, hierarchy):
        text = f"X: {center_x - FRAME_CENTER[0]}, Y: {center_y - FRAME_CENTER[1]}"
        cv2.putText(img, text, (center_x, center_y + 30), cv2.FONT_HERSHEY_SIMPLEX, 0.4,
                    (0, 50, 255), 1)
        cv2.circle(img, (center_x, center_y), 2, GREEN_COLOR, -1, 8, 0)
        cv2.drawContours(img, contours, idx, RED_COLOR, 2, 8, hierarchy, 0)

        return img

    @staticmethod
    def _draw_detected_square_center_point(img, center_x, center_y, contour, idx, hierarchy):
        cv2.drawContours(img, contour, idx, GREEN_COLOR, 2, 8, hierarchy, 0)
        cv2.circle(img, (center_x, center_y), 4, WHITE_COLOR, -1, 8, 0)

        return img

    def _draw_tvl_info(self, img, min_spec, max_spec):
        for idx, key in enumerate(self.tvl_roi_crop.keys()):
            # Get ROI
            x = self.tvl_roi_crop[key][0]
            y = self.tvl_roi_crop[key][1]
            w = self.tvl_roi_crop[key][2]
            h = self.tvl_roi_crop[key][3]

            # Specify line/text color
            if min_spec[idx] <= self.tvl_vals[key] <= max_spec[idx]:
                line_color = GREEN_COLOR
            else:
                line_color = RED_COLOR

            # Draw line and text
            if key == 0:  # CR
                start_point = (x + self.tvl_locations[key], y + 1)
                end_point = (x + self.tvl_locations[key], y + TVL_ROI_WIDTH - 1)
                txt_output = f"{int(self.tvl_vals[key])}"
                txt_point = (int(x + H_TVL_ROI_HEIGHT + 5), int(y + (TVL_ROI_WIDTH / 2)))
            elif key == 1:  # CL
                start_point = (x + (H_TVL_ROI_HEIGHT - self.tvl_locations[key]), y + 1)
                end_point = (x + (H_TVL_ROI_HEIGHT - self.tvl_locations[key]), y + TVL_ROI_WIDTH - 1)
                txt_output = f"{int(self.tvl_vals[key])}"
                txt_point = (int(x - 30), int(y + (TVL_ROI_WIDTH / 2)))
            elif key == 2:  # CT
                start_point = (x + 1, y + (V_TVL_ROI_HEIGHT - self.tvl_locations[key]))
                end_point = (x + TVL_ROI_WIDTH - 1, y + (V_TVL_ROI_HEIGHT - self.tvl_locations[key]))
                txt_output = f"{int(self.tvl_vals[key])}"
                txt_point = (int(x + 8), int(y - 5))
            elif key == 3:  # CB
                start_point = (x + 1, y + self.tvl_locations[key])
                end_point = (x + TVL_ROI_WIDTH - 1, y + self.tvl_locations[key])
                txt_output = f"{int(self.tvl_vals[key])}"
                txt_point = (int(x + 8), int(y + V_TVL_ROI_HEIGHT + 13))
            else:
                start_point = (x + self.tvl_locations[key], y + 1)
                end_point = (x + self.tvl_locations[key], y + TVL_ROI_WIDTH - 1)
                txt_output = f"{int(self.tvl_vals[key])}"
                txt_point = (int(x + H_TVL_ROI_HEIGHT + 5), int(y + (TVL_ROI_WIDTH / 2)))
            cv2.line(img, start_point, end_point, line_color, 2, cv2.LINE_AA)
            cv2.putText(img, txt_output, txt_point, cv2.FONT_HERSHEY_SIMPLEX, 0.4, line_color, 1)

            # Draw rectangle ROI
            start_point = (x, y)
            end_point = (x + w, y + h)
            cv2.rectangle(img, start_point, end_point, BLUE_COLOR, 2)

        return img

    def _draw_fov_info(self, img, min_spec, max_spec):
        for idx, key in enumerate(self.fov_roi_crop.keys()):
            # Get ROI
            x = self.fov_roi_crop[key][0]
            y = self.fov_roi_crop[key][1]
            w = self.fov_roi_crop[key][2]
            h = self.fov_roi_crop[key][3]

            if self.fov_vals[key] is not None:
                # Specify line/text color
                if min_spec[idx] <= self.fov_vals[key] <= max_spec[idx]:
                    line_color = GREEN_COLOR
                else:
                    line_color = RED_COLOR

                # Draw line and text
                if key == LC:
                    txt_output = f"{int(self.fov_vals[key])}"
                    txt_point = (int(x - 30), int(y + (FOV_ROI_WIDTH / 2)))
                elif key == RC:
                    txt_output = f"{int(self.fov_vals[key])}"
                    txt_point = (int(x + H_FOV_ROI_HEIGHT + 5), int(y + (FOV_ROI_WIDTH / 2)))
                elif key == TC:
                    txt_output = f"{int(self.fov_vals[key])}"
                    # txt_point = (int(x + 8), int(y - 5))
                    txt_point = (int(x - 30), int(y + (FOV_ROI_WIDTH / 2)))
                elif key == BC:
                    txt_output = f"{int(self.fov_vals[key])}"
                    # txt_point = (int(x + 8), int(y + H_FOV_ROI_HEIGHT + 13))
                    txt_point = (int(x - 30), int(y + (FOV_ROI_WIDTH / 2)))
                else:
                    txt_output = f"{int(self.fov_vals[key])}"
                    txt_point = (int(x - 30), int(y + (FOV_ROI_WIDTH / 2)))
                cv2.putText(img, txt_output, txt_point, cv2.FONT_HERSHEY_SIMPLEX, 0.4, line_color, 1)

                # Draw circle center point
                center_x = self.fov_locations[key][0]
                center_y = self.fov_locations[key][1]
                cv2.circle(img, (center_x, center_y), 2, GREEN_COLOR, -1, 8, 0)

            # Draw rectangle ROI
            start_point = (x, y)
            end_point = (x + w, y + h)
            cv2.rectangle(img, start_point, end_point, BLUE_COLOR, 2)

        return img

    @staticmethod
    def _draw_rotation_info(img, rotated_contour, rotated_angle):
        # Draw box between color boxes
        img = cv2.drawContours(img, [rotated_contour], -1, (0, 0, 0))

        rotated_angle_str = "{:.2f}".format(rotated_angle)
        txt_output = f"Rotation: {rotated_angle_str} deg"

        # Get bottom-right position
        box_center_point_list = rotated_contour.tolist()
        x, y = box_center_point_list[0][0][0], box_center_point_list[0][0][1]

        # Draw text
        img = cv2.putText(img, txt_output, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.4, BLUE_COLOR, 1)

        return img

    @staticmethod
    def _draw_color_info(img, rotated_contour, delta_blue, delta_green, delta_red):
        box_center_list = rotated_contour.tolist()

        txt_red = f"delta-RED: {delta_red}"
        txt_green = f"delta-GREEN: {delta_green}"
        txt_blue = f"delta-BLUE: {delta_blue}"

        # Put text
        cv2.putText(img, txt_red, (box_center_list[3][0][0], box_center_list[3][0][1] - 15),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.4, WHITE_COLOR, 1)
        cv2.putText(img, txt_green, (box_center_list[1][0][0], box_center_list[1][0][1] + 15),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.4, WHITE_COLOR, 1)
        cv2.putText(img, txt_blue, (box_center_list[2][0][0], box_center_list[2][0][1] - 15),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.4, WHITE_COLOR, 1)

        return img

    @staticmethod
    def _draw_mirror_info(img, mirrored, mirrored_str):
        if mirrored:
            color = BLUE_COLOR
        else:
            color = RED_COLOR

        cv2.putText(img, mirrored_str, (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.4, color, 1)

        return img

    def _draw_overlay_info(self, img):
        for key in self.overlay_roi_crop.keys():
            roi_x = self.overlay_roi_crop[key][0]
            roi_y = self.overlay_roi_crop[key][1]
            roi_width = self.overlay_roi_crop[key][2]
            roi_height = self.overlay_roi_crop[key][3]

            # Specify color
            if self.overlay_exists[key]:
                roi_color = GREEN_COLOR
            else:
                roi_color = RED_COLOR

            # Draw rectangle ROI
            start_point = (roi_x, roi_y)
            end_point = (roi_x + roi_width, roi_y + roi_height)
            cv2.rectangle(img, start_point, end_point, roi_color, 2)

        return img

    @staticmethod
    def _draw_parking_guideline_info(img, pgl_detection_dict, pgl_result, pgl_contained_result_list):
        # Color
        if pgl_result is not None:
            if pgl_result:
                box_color = GREEN_COLOR
            else:
                box_color = RED_COLOR
        else:
            box_color = RED_COLOR

        # PGL detection chart
        for idx, key in enumerate(pgl_detection_dict.keys(), 1):
            center_x = pgl_detection_dict[key][0]
            center_y = pgl_detection_dict[key][1]
            # x = pgl_detection_dict[key][3]
            # y = pgl_detection_dict[key][4]
            # w = pgl_detection_dict[key][5]
            # h = pgl_detection_dict[key][6]
            r = pgl_detection_dict[key][7]

            if len(pgl_contained_result_list) != 0:
                if pgl_contained_result_list[idx - 1]:
                    box_color = GREEN_COLOR
                else:
                    box_color = RED_COLOR

            # cv2.putText(img, str(idx), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.4, box_color, 1)
            cv2.circle(img, (int(center_x), int(center_y)), r, box_color, 1)
            # cv2.rectangle(img, (x, y), (x + w, y + h), box_color, 1)

        # PGL extreme point
        for pgl_point in ALL_PGL_POINTS:
            for point in pgl_point:
                x = point[0]
                y = point[1]
                cv2.circle(img, (x, y), 0, RED_COLOR, 2)

        return img

    def _specify_overlay_exists(self):
        result_list = []
        for key in self.overlay_vals.keys():
            blue = self.overlay_vals[key][0]
            green = self.overlay_vals[key][1]
            red = self.overlay_vals[key][2]

            if key == ROI_TOP:
                min_blue = OVERLAY_TOP_BLUE_SPEC[0]
                max_blue = OVERLAY_TOP_BLUE_SPEC[1]
                min_green = OVERLAY_TOP_GREEN_SPEC[0]
                max_green = OVERLAY_TOP_GREEN_SPEC[1]
                min_red = OVERLAY_TOP_RED_SPEC[0]
                max_red = OVERLAY_TOP_RED_SPEC[1]
            elif key == ROI_CENTER:
                min_blue = OVERLAY_CENTER_BLUE_SPEC[0]
                max_blue = OVERLAY_CENTER_BLUE_SPEC[1]
                min_green = OVERLAY_CENTER_GREEN_SPEC[0]
                max_green = OVERLAY_CENTER_GREEN_SPEC[1]
                min_red = OVERLAY_CENTER_RED_SPEC[0]
                max_red = OVERLAY_CENTER_RED_SPEC[1]
            else:  # BOTTOM
                min_blue = OVERLAY_BOTTOM_BLUE_SPEC[0]
                max_blue = OVERLAY_BOTTOM_BLUE_SPEC[1]
                min_green = OVERLAY_BOTTOM_GREEN_SPEC[0]
                max_green = OVERLAY_BOTTOM_GREEN_SPEC[1]
                min_red = OVERLAY_BOTTOM_RED_SPEC[0]
                max_red = OVERLAY_BOTTOM_RED_SPEC[1]

            # Specify each color BGR
            if min_blue <= blue <= max_blue:
                result_blue = True
            else:
                # print(f"{key}: blue: {min_blue}, {blue}, {max_blue}")
                result_blue = False
            if min_green <= green <= max_green:
                result_green = True
            else:
                # print(f"{key}: green: {min_green}, {green}, {max_green}")
                result_green = False
            if min_red <= red <= max_red:
                result_red = True
            else:
                # print(f"{key}: red: {min_red}, {red}, {max_red}")
                result_red = False

            if result_blue and result_green and result_red:
                result_list.append(True)
                self.overlay_exists[key] = True
            else:
                result_list.append(False)
                self.overlay_exists[key] = False

        # Specify result
        if all(result_list):
            overlay_exists = True
        else:
            overlay_exists = False

        return overlay_exists

    @staticmethod
    def plot_local_max_min(avg_pixel_np_array):
        # Find peaks(max).
        peak_indexes = argrelextrema(avg_pixel_np_array, np.greater)
        peak_indexes = peak_indexes[0]

        # Find valleys(min).
        valley_indexes = argrelextrema(avg_pixel_np_array, np.less)
        valley_indexes = valley_indexes[0]

        # Plot main graph.
        (fig, ax) = plt.subplots()
        # ax.plot(avg_pixel_np_array, avg_pixel_np_array)

        # Plot peaks.
        peak_x = peak_indexes
        peak_y = avg_pixel_np_array[peak_indexes]
        ax.plot(peak_x, peak_y, marker='o', linestyle='dashed', color='green', label="Peaks")

        # Plot valleys.
        valley_x = valley_indexes
        valley_y = avg_pixel_np_array[valley_indexes]
        ax.plot(valley_x, valley_y, marker='o', linestyle='dashed', color='red', label="Valleys")

        # Save graph to file.
        plt.title('Find peaks and valleys using argrelextrema()')
        plt.legend(loc='best')
        plt.show()
        # plt.savefig('argrelextrema.png')

    @staticmethod
    def plot_contrast_col(val_list, title=None):
        # Plot main graph.
        (fig, ax) = plt.subplots()

        # Plot
        x_idx = [idx for idx, i in enumerate(val_list)]
        y_idx = val_list
        if title is not None:
            label = title
        else:
            label = "Contrast"
        ax.plot(x_idx, y_idx, marker='o', linestyle='dashed', color='green', label="Contrast")

        # Save graph to file.
        plt.title(label)
        plt.legend(loc='best')
        plt.show()
